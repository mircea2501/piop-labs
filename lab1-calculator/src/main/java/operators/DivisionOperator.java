package operators;

/**
 * An {@link ArithmeticOperator} that calculates the division between operands.
 */
public class DivisionOperator extends ArithmeticOperator {

    /**
     * Calculates the division between the two operands.

     * @return double value representing the result
     */
    @Override
    public double calculate(double operand1, double operand2) {
        return operand1 / operand2;
    }
}
