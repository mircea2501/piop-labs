package operators;

/**
 * An {@link ArithmeticOperator} that calculates the subtraction between operands.
 */
public class SubtractionOperator extends ArithmeticOperator {

    /**
     * Calculates the subtraction between two operands.

     * @return double value representing the result
     */
    @Override
    public double calculate(double operand1, double operand2) {
        return operand1 - operand2;
    }
}
