package operators;

/**
 * An {@link ArithmeticOperator} that calculates the addition between operands.
 */
public class AdditionOperator extends ArithmeticOperator {

    /**
     * Calculates the addition between the two operands.

     * @return double value representing the result
     */
    @Override
    public double calculate(double operand1, double operand2) {
        return operand1 + operand2;
    }
}
