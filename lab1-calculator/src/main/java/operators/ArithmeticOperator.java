package operators;

import java.util.HashMap;

/**
 * An arithmetic operator uses the method {@link #calculate(double, double) calculate} on two double operands.
 * Classes that implement this interface must implement this method.
 */
public abstract class ArithmeticOperator {

    private static HashMap<Class<?>, Integer> operatorOrderValues;

    static {
        operatorOrderValues = new HashMap<>();
        operatorOrderValues.put(MultiplicationOperator.class, 0);
        operatorOrderValues.put(DivisionOperator.class, 0);
        operatorOrderValues.put(AdditionOperator.class, 1);
        operatorOrderValues.put(SubtractionOperator.class, 1);
    }

    /**
     * Calculates the result of an arithmetic computation between the two operands.
     *
     * @return double value representing the result
     */
    public abstract double calculate(double operand1, double operand2);

    /**
     * Compares two {@link ArithmeticOperator} objects by their precedence in an arithmetic expression.

     * @return a negative integer if op1 precedes op2, 0 if they are equal or a positive integer if op2 precedes op1
     */
    public static int compare(ArithmeticOperator op1, ArithmeticOperator op2) {
        return Integer.compare(operatorOrderValues.get(op1.getClass()), operatorOrderValues.get(op2.getClass()));
    }

    public static ArithmeticOperator valueOf(String opString) {
        switch (opString) {
            case "+":
                return new AdditionOperator();
            case "-":
                return new SubtractionOperator();
            case "*":
                return new MultiplicationOperator();
            case "/":
                return new DivisionOperator();
            default:
                return null;
        }
    }
}
