package operators;

/**
 * An {@link ArithmeticOperator} that calculates the product of the operands.
 */
public class MultiplicationOperator extends ArithmeticOperator {

    /**
     * Calculates the product of the two operands.

     * @return double value representing the result
     */
    @Override
    public double calculate(double operand1, double operand2) {
        return operand1 * operand2;
    }
}
