package utils;

/**
 * Utility class used for printing output.
 */
public class Printer {

    public static void consolePrintResult(double result) {
        System.out.println("Result: " + result);
    }

    public static void consolePrintError(Exception e) {
        System.err.println(e.getMessage());
    }
}
