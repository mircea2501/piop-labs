package utils.parser.validator;

/**
 * Any validator should implement this interface.
 * @param <E> the type of object it validates
 */
public interface Validator<E> {
    /**
     * Verify if the object is valid.
     * @param object the object to be validated
     * @throws ValidationError if the object is not valid
     */
    void validate(E object) throws ValidationError;
}
