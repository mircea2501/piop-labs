package utils.parser.validator;

/**
 * Exception thrown by validators.
 */
public class ValidationError extends Exception {
    public ValidationError(String message) {
        super(message);
    }
}
