package utils.parser.validator;

import java.util.regex.Pattern;

/**
 * This class is used for validating an infix arithmetic expression.
 */
public class InfixArithmeticExpressionValidator implements Validator<String> {
    private static final Pattern expressionPattern =
            Pattern.compile("^((([+-]?[1-9]+(\\.[0-9]+)?) [+*-/] )+([+-]?[1-9]+(\\.[0-9]+)?))$");

    /**
     * Checks if a given string matches the pattern of an infix arithmetic expression.
     * @param object the object to be validated
     * @throws ValidationError if validation fails
     */
    @Override
    public void validate(String object) throws ValidationError {
        if (!expressionPattern.matcher(object).matches()) {
            throw new ValidationError("Infix arithmetic expression pattern not respected!");
        }
    }
}
