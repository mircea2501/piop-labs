package utils.parser;

import java.util.List;

/**
 * Any class that inherits this interface must implement the static method {@link #parse(String) parse}.
 */
public interface Parser {
    /**
     * Parses a string into tokens.
     *
     * @param inputString the string that is parsed
     *
     * @return a list of {@link String} tokens
     *
     * @throws ParserError if the input string cannot be parsed
     */
    List<String> parse(String inputString) throws ParserError;
}
