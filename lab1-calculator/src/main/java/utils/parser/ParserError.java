package utils.parser;

/**
 * Exception thrown by objects that implement the interface {@link Parser}.
 */
public class ParserError extends Exception {
    public ParserError(String message) {
        super(message);
    }
}
