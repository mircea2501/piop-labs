package utils.parser;

import utils.parser.validator.InfixArithmeticExpressionValidator;
import utils.parser.validator.ValidationError;

import java.util.Arrays;
import java.util.List;

/**
 * This class is used for parsing strings that are an arithmetic expression in its infix form.
 */
public class InfixArithmeticExpressionParser implements ArithmeticExpressionParser {
    private static final InfixArithmeticExpressionParser instance = null;
    private final InfixArithmeticExpressionValidator validator;

    private InfixArithmeticExpressionParser() {
        validator = new InfixArithmeticExpressionValidator();
    }

    public static InfixArithmeticExpressionParser getInstance() {
        if (instance != null) return instance;
        return new InfixArithmeticExpressionParser();
    }

    /**
     * Overrides the method from {@link Parser}. Parses an arithmetic expression written in its infix form.
     *
     * @param inputString the string that is parsed
     *
     * @return list of tokens representing operators or operands
     *
     * @throws ParserError if the input string is not a valid infix arithmetic expression
     */
    @Override
    public List<String> parse(String inputString) throws ParserError {
        try {
            validator.validate(inputString);
            return Arrays.stream(inputString.split(" ")).map(String::strip).toList();
        } catch (ValidationError e) {
            throw new ParserError("String cannot be parsed.\nReason: " + e.getMessage());
        }
    }
}
