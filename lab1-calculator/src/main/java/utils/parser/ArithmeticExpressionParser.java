package utils.parser;

/**
 * An Arithmetic Expression Parser is used for parsing an arithmetic expression.
 */
public interface ArithmeticExpressionParser extends Parser {
}
