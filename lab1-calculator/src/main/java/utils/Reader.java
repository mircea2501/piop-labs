package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Utility class used for reading input
 */
public class Reader {

    public static String consoleReadArithmeticExpression() throws IOException {
        System.out.println("Write expression: ");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        return reader.readLine();
    }
}
