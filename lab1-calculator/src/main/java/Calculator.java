import operators.*;
import utils.parser.ParserError;

import java.util.List;
import java.util.Stack;
import java.util.regex.Pattern;

/**
 * A calculator computes the result of an arithmetic expression
 */
public class Calculator {

    private static Calculator instance = null;
    private final Stack<Double> operands;
    private final Stack<ArithmeticOperator> operators;
    private final Pattern operandPattern;

    public static Calculator getInstance() {
        if (instance == null) {
            instance = new Calculator();
        }
        return instance;
    }

    private Calculator() {
        operands = new Stack<>();
        operators = new Stack<>();
        operandPattern = Pattern.compile("^([+-]?[1-9]+(\\.[0-9]+)?)$");
    }

    public double calculate(List<String> tokens) {
        for (String token : tokens) {
            if (operandPattern.matcher(token).matches()) {
                operands.push(Double.valueOf(token));
            }
            else {
                ArithmeticOperator operator = ArithmeticOperator.valueOf(token);
                while (!operators.empty() && ArithmeticOperator.compare(operators.peek(), operator) <= 0) {
                    double op2 = operands.pop();
                    double op1 = operands.pop();
                    operands.push(operators.pop().calculate(op1, op2));
                }
                operators.push(operator);
            }
        }

        while (!operators.empty()) {
            double op2 = operands.pop();
            double op1 = operands.pop();
            operands.push(operators.pop().calculate(op1, op2));
        }

        return operands.pop();
    }
}
