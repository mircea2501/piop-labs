import utils.Printer;
import utils.Reader;
import utils.parser.InfixArithmeticExpressionParser;
import utils.parser.Parser;

import java.util.List;

public class App {
    private static final Parser parser = InfixArithmeticExpressionParser.getInstance();
    private static final Calculator calculator = Calculator.getInstance();

    public static void main(String[] args) {
        boolean finished = false;
        while (!finished) {
            try {
                String expression = Reader.consoleReadArithmeticExpression();
                List<String> tokens = parser.parse(expression);
                double result = calculator.calculate(tokens);
                Printer.consolePrintResult(result);
            } catch (Exception e) {
                Printer.consolePrintError(e);
                finished = true;
            }

        }
    }
}
