package operators;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MultiplicationOperatorTest {

    private ArithmeticOperator operator;

    @Before
    public void setUp() throws Exception {
        operator = new MultiplicationOperator();
    }

    @Test
    public void shouldReturnProduct() {
        assertEquals(417.15, operator.calculate(15.45, 27), 0.001);
    }
}