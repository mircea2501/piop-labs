package operators;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ArithmeticOperatorTest {

    private ArithmeticOperator op1;
    private ArithmeticOperator op2;
    private ArithmeticOperator op3;

    @Before
    public void setup() {
        op1 = new DivisionOperator();
        op2 = new MultiplicationOperator();
        op3 = new AdditionOperator();
    }

    @Test
    public void shouldBeLower() {
        assertTrue(ArithmeticOperator.compare(op1, op3) < 0);
    }

    @Test
    public void shouldBeEqual() {
        assertEquals(0, ArithmeticOperator.compare(op1, op2));
    }
}