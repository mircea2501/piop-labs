package operators;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SubtractionOperatorTest {

    private ArithmeticOperator operator;

    @Before
    public void setUp() throws Exception {
        operator = new SubtractionOperator();
    }

    @Test
    public void shouldReturnDifference() {
        assertEquals(-11, operator.calculate(12.2, 23.2), 0.001);
    }
}