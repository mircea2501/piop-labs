package operators;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AdditionOperatorTest {

    private ArithmeticOperator operator;

    @Before
    public void setUp() {
        operator = new AdditionOperator();
    }

    @Test
    public void shouldReturnSum() {
        assertEquals(5.5, operator.calculate(2.2, 3.3), 0.001);
    }
}