package operators;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DivisionOperatorTest {

    private ArithmeticOperator operator;

    @Before
    public void setUp() throws Exception {
        operator = new DivisionOperator();
    }

    @Test
    public void shouldReturnDivisionResult() {
        assertEquals(12.644, operator.calculate(94.2, 7.45), 0.01);
    }
}