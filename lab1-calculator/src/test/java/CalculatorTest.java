import org.junit.Before;
import org.junit.Test;
import utils.parser.InfixArithmeticExpressionParser;
import utils.parser.ParserError;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class CalculatorTest {
    private Calculator calculator;
    private List<String> simpleExpression;
    private List<String> compoundExpression;
    private List<String> orderOfOperatorsExpression1;
    private List<String> orderOfOperatorsExpression2;
    private final double delta = 0.001;

    @Before
    public void setUp() {
        calculator = Calculator.getInstance();
        InfixArithmeticExpressionParser parser = InfixArithmeticExpressionParser.getInstance();
        try {
            simpleExpression = parser.parse("2 + 3");
            compoundExpression = parser.parse("2 + 3 + 5");
            orderOfOperatorsExpression1 = parser.parse("2 + 3 - 2");
            orderOfOperatorsExpression2 = parser.parse("2 * 3 + 4");
        } catch (ParserError e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void testSimpleExpression() {
        assertEquals(5, calculator.calculate(simpleExpression), delta);
    }

    @Test
    public void testCompoundExpression() {
        assertEquals(10, calculator.calculate(compoundExpression), delta);
    }

    @Test
    public void testOrderOfOperations1() {
        assertEquals(3, calculator.calculate(orderOfOperatorsExpression1), delta);
    }

    @Test
    public void testOrderOfOperations2() {
        assertEquals(10, calculator.calculate(orderOfOperatorsExpression2), delta);
    }
}