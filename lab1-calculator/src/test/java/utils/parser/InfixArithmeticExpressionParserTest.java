package utils.parser;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class InfixArithmeticExpressionParserTest {

    private InfixArithmeticExpressionParser parser;

    @Before
    public void setup() {
        parser = InfixArithmeticExpressionParser.getInstance();
    }

    @Test
    public void shouldReturnTokensIfValidExpression() {
        try {
            var tokens = parser.parse("3 + -2 / 3.14");
            assertEquals(tokens.get(0), "3");
            assertEquals(tokens.get(1), "+");
            assertEquals(tokens.get(2), "-2");
            assertEquals(tokens.get(3), "/");
            assertEquals(tokens.get(4), "3.14");
        } catch (ParserError e) {
            fail();
        }
    }

    @Test
    public void shouldThrowErrorIfInvalidExpression() {
        try {
            parser.parse("4 - 2.14 /");
            fail();
        } catch (ParserError e) {
            assertTrue(true);
        }
    }
}