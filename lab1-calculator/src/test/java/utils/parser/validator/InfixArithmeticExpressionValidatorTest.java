package utils.parser.validator;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class InfixArithmeticExpressionValidatorTest {

    private InfixArithmeticExpressionValidator validator;

    @Before
    public void setup() {
        validator = new InfixArithmeticExpressionValidator();
    }

    @Test
    public void shouldValidateCorrectExpression() {
        try {
            validator.validate("2.3 + 3 / 5 - +3 * -3");
            assertTrue(true);
        } catch (ValidationError e) {
            fail();
        }
    }

    @Test
    public void shouldThrowExceptionForIncorrectExpression() {
        try {
            validator.validate("2.3 + 3 -");
            fail();
        } catch (ValidationError e) {
            assertTrue(true);
        }
    }
}