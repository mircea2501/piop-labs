package org.tora.model;

import java.util.concurrent.ThreadLocalRandom;

public class OrderFactory {

    public static Order getNewById(int id) {
        int price = ThreadLocalRandom.current().nextInt(1, 200);
        int quantity = ThreadLocalRandom.current().nextInt(1, 1000);
        return new Order(id, price, quantity);
    }

    public static Order getNew(int size) {
        int price = ThreadLocalRandom.current().nextInt(1, 200);
        int quantity = ThreadLocalRandom.current().nextInt(1, 1000);
        return new Order(size+1, price, quantity);
    }

    public static Order getExistingOrNew(int size) {
        int id = ThreadLocalRandom.current().nextInt(1, size*2);
        int price = ThreadLocalRandom.current().nextInt(1, 200);
        int quantity = ThreadLocalRandom.current().nextInt(1, 1000);
        return new Order(id, price, quantity);
    }
}
