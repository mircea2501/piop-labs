package org.tora.repository;

import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.impl.factory.Lists;

public class EclipseListBasedRepository<T> implements InMemoryRepository<T> {
    private MutableList<T> list;

    public EclipseListBasedRepository() {
        this.list = Lists.mutable.empty();
    }

    @Override
    public void add(T elem) {
        list.add(elem);
    }

    @Override
    public boolean contains(T elem) {
        return list.contains(elem);
    }

    @Override
    public void remove(T elem) {
        list.remove(elem);
    }
}
