package org.tora.repository;

import gnu.trove.set.hash.THashSet;

public class Trove4JHashSetRepository<T> implements InMemoryRepository<T> {
    private THashSet<T> set;

    public Trove4JHashSetRepository() {
        this.set = new THashSet<>();
    }

    @Override
    public void add(T elem) {
        set.add(elem);
    }

    @Override
    public boolean contains(T elem) {
        return set.contains(elem);
    }

    @Override
    public void remove(T elem) {
        set.remove(elem);
    }
}
