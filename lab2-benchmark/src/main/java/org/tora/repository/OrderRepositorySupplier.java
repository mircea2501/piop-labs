package org.tora.repository;

import org.tora.model.Order;

public enum OrderRepositorySupplier {
    ARRAY_LIST(new ArrayListBasedRepository<>()),
    HASH_SET(new HashSetBasedRepository<>()),
    TREE_SET(new TreeSetBasedRepository<>()),
    CONCURRENT_HASH_MAP(new ConcurrentHashMapBasedRepository<>()),
    ECLIPSE_LIST(new EclipseListBasedRepository<>()),
    TROVE4J_HASH_SET(new Trove4JHashSetRepository<>());
    private final InMemoryRepository<Order> orderRepository;

    OrderRepositorySupplier(InMemoryRepository<Order> repository) {
        this.orderRepository = repository;
    }

    public InMemoryRepository<Order> valueOf() {
        return orderRepository;
    }
}
