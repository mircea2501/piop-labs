package org.tora.repository;

import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapBasedRepository<T> implements InMemoryRepository<T> {
    private final ConcurrentHashMap<T, T> map;

    public ConcurrentHashMapBasedRepository() {
        this.map = new ConcurrentHashMap<>();
    }

    @Override
    public void add(T elem) {
        map.put(elem, elem);
    }

    @Override
    public boolean contains(T elem) {
        return map.containsKey(elem);
    }

    @Override
    public void remove(T elem) {
        map.remove(elem);
    }
}
