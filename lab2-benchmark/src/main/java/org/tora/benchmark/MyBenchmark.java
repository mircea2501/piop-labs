/*
 * Copyright (c) 2014, Oracle America, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 *  * Neither the name of Oracle nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.tora.benchmark;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.tora.model.Order;
import org.tora.model.OrderFactory;
import org.tora.repository.InMemoryRepository;
import org.tora.repository.OrderRepositorySupplier;

public class MyBenchmark {

    @State(Scope.Benchmark)
    public static class RepoState {

        @Param({"ARRAY_LIST", "HASH_SET", "TREE_SET", "CONCURRENT_HASH_MAP", "ECLIPSE_LIST", "TROVE4J_HASH_SET"})
        public OrderRepositorySupplier supplier;
        public InMemoryRepository<Order> ordersRepo;

        @Param({"10", "10000", "1000000"})
        public int size;

        @Setup(Level.Iteration)
        public void setUp() {
            ordersRepo = supplier.valueOf();
            for (int i = 1; i <= size; i++) {
                ordersRepo.add(OrderFactory.getNewById(i));
            }
        }
    }

    @State(Scope.Benchmark)
    public static class AddState {
        public Order order;

        @Setup(Level.Invocation)
        public void generateOrder(RepoState repoState) {
            order = OrderFactory.getNew(repoState.size);
        }

        @TearDown(Level.Invocation)
        public void removeOrder(RepoState repoState) {
            repoState.ordersRepo.remove(order);
        }
    }

    @State(Scope.Benchmark)
    public static class ContainsState {
        public Order order;

        @Setup(Level.Invocation)
        public void generateOrder(RepoState repoState) {
            order = OrderFactory.getExistingOrNew(repoState.size);
        }
    }

    @State(Scope.Benchmark)
    public static class RemovesState {
        public Order order;
        private boolean exists;

        @Setup(Level.Invocation)
        public void generateOrder(RepoState repoState) {
            order = OrderFactory.getExistingOrNew(repoState.size);
            exists = repoState.ordersRepo.contains(order);
        }

        @TearDown(Level.Invocation)
        public void addOrder(RepoState repoState) {
            if (exists) {
                repoState.ordersRepo.add(order);
            }
        }
    }

    @Benchmark
    public void addTest(RepoState repoState, AddState addState) {
        repoState.ordersRepo.add(addState.order);
    }

    @Benchmark
    public void containsTest(RepoState repoState, ContainsState addState, Blackhole blackhole) {
        blackhole.consume(repoState.ordersRepo.contains(addState.order));
    }

    @Benchmark
    public void removeTest(RepoState repoState, RemovesState removesState) {
        repoState.ordersRepo.remove(removesState.order);
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(MyBenchmark.class.getSimpleName())
                .forks(1)
                .warmupIterations(5)
                .measurementIterations(5)
                .resultFormat(ResultFormatType.TEXT)
                .result("instrumented_output.txt")
                .build();

        new Runner(opt).run();
    }
}
