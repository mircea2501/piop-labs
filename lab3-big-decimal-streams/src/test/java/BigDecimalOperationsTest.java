import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class BigDecimalOperationsTest {
    private List<BigDecimal> bigDecimals;
    private BigDecimal sum;
    private BigDecimal avg;

    @Before
    public void setUp() throws Exception {
        bigDecimals = new ArrayList<>();
        sum = BigDecimal.valueOf(0);
        for (int i = 0; i < 100000; i++) {
            BigDecimal bigDecimal = BigDecimal.valueOf(Math.sqrt(i));
            bigDecimals.add(bigDecimal);
            sum = sum.add(bigDecimal);
        }
        bigDecimals.add(null);
        avg = sum.divide(BigDecimal.valueOf(bigDecimals.size()), RoundingMode.UP);
    }

    @Test
    public void sum() {
        assertNull(BigDecimalOperations.sum(null));
        assertEquals(sum, BigDecimalOperations.sum(bigDecimals));
    }

    @Test
    public void avg() {
        assertNull(BigDecimalOperations.avg(null));
        assertEquals(BigDecimal.ZERO, BigDecimalOperations.avg(new ArrayList<BigDecimal>()));
        assertEquals(avg, BigDecimalOperations.avg(bigDecimals));
    }

//    @Test
//    public void top10Percent() {
//        assertNull(BigDecimalOperations.top10Percent(null));
//
//        List<BigDecimal> testList = new ArrayList<>();
//        for (int i = 99999; i >= 90000; i--) {
//            testList.add(BigDecimal.valueOf(Math.sqrt(i)));
//        }
//
//        List<BigDecimal> calculatedList = BigDecimalOperations.top10Percent(bigDecimals);
//
//        assertEquals(testList.size(), calculatedList.size());
//
//        for (int i = 0; i < calculatedList.size(); i++) {
//            assertEquals(testList.get(i), calculatedList.get(i));
//        }
//    }

    @Test
    public void serializeAndDeserialize() {
        List<BigDecimal> resultList = BigDecimalOperations.serializeAndDeserialize(bigDecimals);
        assertEquals(bigDecimals.size(), resultList.size());

        for (int i = 0; i < resultList.size(); i++) {
            assertEquals(bigDecimals.get(i), resultList.get(i));
        }
    }
}