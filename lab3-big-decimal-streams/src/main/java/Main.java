import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Main {
    private static List<BigDecimal> initialiseList(int size) {
        List<BigDecimal> bigDecimalList = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            bigDecimalList.add(BigDecimal.valueOf(Math.random()));
        }
        return bigDecimalList;
    }

    public static void main(String[] args) {
        List<BigDecimal> bigDecimalList = initialiseList(20);
        System.out.println("Numbers before serialization:");
        bigDecimalList.forEach(System.out::println);

        List<BigDecimal> serializedDeserializedBigDecimalList =
                BigDecimalOperations.serializeAndDeserialize(bigDecimalList);
        System.out.println("\nNumbers after serialization and deserialization:");
        serializedDeserializedBigDecimalList.forEach(System.out::println);

        System.out.println("\nSum: " + BigDecimalOperations.sum(serializedDeserializedBigDecimalList));

        System.out.println("\nAverage: " + BigDecimalOperations.avg(serializedDeserializedBigDecimalList));

        System.out.println("\nTop 10% biggest numbers:");
        BigDecimalOperations.printTop10Percent(serializedDeserializedBigDecimalList);
    }
}
