import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class BigDecimalOperations {
    public static BigDecimal sum(List<BigDecimal> bigDecimalList) {
        if (bigDecimalList == null) {
            return null;
        }
        return bigDecimalList.stream()
                .filter(Objects::nonNull)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public static BigDecimal avg(List<BigDecimal> bigDecimalList) {
        if (bigDecimalList == null) {
            return null;
        }

        if (bigDecimalList.size() == 0) {
            return BigDecimal.ZERO;
        }

        return bigDecimalList.stream()
                .filter(Objects::nonNull)
                .reduce(BigDecimal.ZERO, BigDecimal::add)
                .divide(BigDecimal.valueOf(bigDecimalList.size()), RoundingMode.UP);
    }

    public static void printTop10Percent(List<BigDecimal> bigDecimalList) {
        if (bigDecimalList != null) {
            bigDecimalList.stream()
                    .filter(Objects::nonNull)
                    .sorted(Comparator.reverseOrder())
                    .limit(Double.valueOf(bigDecimalList.size() * 0.1).longValue())
                    .forEach(System.out::println);
        }
    }

    public static List<BigDecimal> serializeAndDeserialize(List<BigDecimal> bigDecimalList) {
        List<BigDecimal> serializedDeserializedList = new ArrayList<>(bigDecimalList.size());
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(bos);
            for (var x : bigDecimalList) {
                oos.writeObject(x);
            }

            ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bis);
            for (int i = 0; i < bigDecimalList.size(); i++) {
                serializedDeserializedList.add((BigDecimal) ois.readObject());
            }
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        return serializedDeserializedList;
    }
}
