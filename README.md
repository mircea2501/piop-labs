PIOP laboratories 
==================

## Multi-module maven project integration with GitLab and Codacy

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/ec25528cf70c4d82b71b13ee1c516afc)](https://www.codacy.com/gl/mircea2501/piop-labs/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=mircea2501/piop-labs&amp;utm_campaign=Badge_Grade)
[![Codacy Badge](https://app.codacy.com/project/badge/Coverage/ec25528cf70c4d82b71b13ee1c516afc)](https://www.codacy.com/gl/mircea2501/piop-labs/dashboard?utm_source=gitlab.com&utm_medium=referral&utm_content=mircea2501/piop-labs&utm_campaign=Badge_Coverage)

This project contains all laboratory assignments solved during the semester.